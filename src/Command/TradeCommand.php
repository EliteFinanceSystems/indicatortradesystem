<?php
declare(strict_types=1);

namespace App\Command;

use App\Dto\StrategyParametersDto;
use App\Service\Strategy\StrategyInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TradeCommand extends Command
{
    protected static $defaultName = 'app:trade';

    /**
     * @var StrategyInterface
     */
    private $strategy;

    public function __construct(
        string $name = null,
        StrategyInterface $strategy
    )
    {
        parent::__construct($name);
        $this->strategy = $strategy;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    final protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $strategyParametersDto = $this->getStrategyParameters($input);
        $this->strategy->run($strategyParametersDto);
        return Command::SUCCESS;
    }

    /**
     * @param InputInterface $input
     * @return StrategyParametersDto
     */
    private function getStrategyParameters(InputInterface $input): StrategyParametersDto
    {
        return new StrategyParametersDto('market', 'instrument', '1H');
    }
}