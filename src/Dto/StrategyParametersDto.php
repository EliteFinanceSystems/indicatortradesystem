<?php
declare(strict_types=1);

namespace App\Dto;

class StrategyParametersDto
{
    /**
     * @var string
     */
    private $market;

    /**
     * @var string
     */
    private $instrument;

    /**
     * @var string
     */
    private $timeFrame;

    /**
     * @param string $market
     * @param string $instrument
     * @param string $timeFrame
     */
    public function __construct(string $market, string $instrument, string $timeFrame)
    {
        $this->market = $market;
        $this->instrument = $instrument;
        $this->timeFrame = $timeFrame;
    }

    /**
     * @return string
     */
    public function getMarket(): string
    {
        return $this->market;
    }

    /**
     * @return string
     */
    public function getInstrument(): string
    {
        return $this->instrument;
    }

    /**
     * @return string
     */
    public function getTimeFrame(): string
    {
        return $this->timeFrame;
    }
}