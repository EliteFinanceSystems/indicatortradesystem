<?php
declare(strict_types=1);

namespace App\Dto;

use DateTimeImmutable;

class SignalDto
{
    /**
     * @var DateTimeImmutable
     */
    private $time;

    /**
     * @var string
     */
    private $position;

    /**
     * @var string
     */
    private $action;

    /**
     * @param string $dateTime
     * @param string $position
     * @param string $action
     */
    public function __construct(string $dateTime, string $position, string $action)
    {
        $this->time = DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $dateTime);
        $this->position = $position;
        $this->action = $action;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getTime(): DateTimeImmutable
    {
        return $this->time;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }
}