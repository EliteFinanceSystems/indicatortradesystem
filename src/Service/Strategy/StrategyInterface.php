<?php
declare(strict_types=1);

namespace App\Service\Strategy;

use App\Dto\StrategyParametersDto;

interface StrategyInterface
{
    /**
     * @param StrategyParametersDto $strategyParametersDto
     */
    public function run(StrategyParametersDto $strategyParametersDto): void;
}