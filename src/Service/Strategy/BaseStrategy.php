<?php
declare(strict_types=1);

namespace App\Service\Strategy;

use App\Dto\SignalDto;
use App\Dto\StrategyParametersDto;

class BaseStrategy implements StrategyInterface
{
    /**
     * {@inheritDoc}
     */
    final public function run(StrategyParametersDto $strategyParametersDto): void
    {
        $lastSignalDto = $this->loadLastSignal($strategyParametersDto);
    }

    /**
     * @param StrategyParametersDto $strategyParametersDto
     * @return SignalDto
     */
    private function loadLastSignal(StrategyParametersDto $strategyParametersDto): SignalDto
    {
        return new SignalDto('2000-01-01 00:00:00', 'LONG', 'OPEN');
    }
}